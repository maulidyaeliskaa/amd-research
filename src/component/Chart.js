import { Line } from "react-chartjs-2";
import {
    Chart as ChartJS,
    LineElement,
    CategoryScale,
    LinearScale,
    PointElement,
    Legend,
    Filler
} from 'chart.js';

ChartJS.register(
    LineElement,
    CategoryScale,
    LinearScale,
    PointElement,
    Legend,
    Filler
)

export default function Chart(){

    const pointImageRed = new Image();
    pointImageRed.src = '/assets/pointStyle/red.svg';
    const pointImageOrange = new Image();
    pointImageOrange.src = '/assets/pointStyle/orange.svg';
    const pointImageGreen = new Image();
    pointImageGreen.src = '/assets/pointStyle/green.svg';
    const pointImageBlue = new Image();
    pointImageBlue.src = '/assets/pointStyle/blue.svg';

    const data = {
        labels: ['Mon', 'Tue', 'Wed'],
        datasets: [
            {
            label: 'Restless',
            data: [0, 5, 9],
            backgroundColor: 'rgba(67, 57, 242, .25)',
            borderColor: 'blue',
            pointBorderColor: 'blue',
            fill: true,
            pointRadius: 0,
            tension: 0.4,
            pointStyle: [pointImageBlue, '', '', pointImageBlue]
        }
    ]
    }

    const options = {
        plugins: {
            title: {
                display: true,
                text: 'Line Diagram',
            },
            legend: {
                labels: {
                    usePointStyle: true,
                }
            }
        },
        scales: {
            y: {
                display: false,
                min: 0,
                max: 12
            },
            x:{
                display: false
            }
        }
    }

    return(
        <div id="LineDiagram">
            <p>Chart</p>
            <Line
                data={data}
                options={options}
            ></Line>
        
        </div>
    )
}