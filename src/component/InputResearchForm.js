export default function InputResearchForm({ onAsignManagerClick, handleChange, onSubmit }) {
  const cancelResearch = () => {
    window.location.href = '/Dashboard';
  };

  return (
    <div id="gHd1cwFNxB">
      <label for="LatarBelakang" className="font-Roboto text-24px">
        Latar Belakang Research*
      </label>
      <textarea name="background_research" onChange={handleChange} placeholder="Jelaskan latar belakang/permasalahan yang dihadapi"></textarea>
      <label for="LatarBelakang" className="font-Roboto text-24px">
        Kategori Research
      </label>
      <textarea name="category_research" onChange={handleChange} type="text" placeholder="Ilmiah / Sejarah / Hukum / Sosial / Budaya"></textarea>
      <label for="LatarBelakang" className="font-Roboto text-24px">
        Goals Research
      </label>
      <textarea name="goal_research" onChange={handleChange} type="text" placeholder="Tentukan tujuan/hasil dari research"></textarea>
      <p className="font-Roboto text-20px">*Wajib diisi</p>
      <div>
        <button type="button" onClick={cancelResearch}>
          Cancel
        </button>
        <button type="submit" onClick={onSubmit}>
          Save
        </button>
        <button type="button" onClick={onAsignManagerClick}>
          Asign a Manager
        </button>
      </div>
    </div>
  );
}
