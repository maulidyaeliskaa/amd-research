import React from 'react'

const NewPasswordForm = ({ onLoginClick }) => {

    function HandleEyeIcon(){
        let password = document.querySelector('.Password');
        let eyeicon = document.getElementById('EyeIcon');

        if(password.type === 'password'){
            password.type = 'text';
            eyeicon.src = "/assets/images/eye-open.png";
        }else{
            password.type = 'password';
            eyeicon.src = "/assets/images/eye-close.png";
        }
    }

    function HandleEyeIconRe(){
        let password = document.querySelector('.PasswordRe');
        let eyeicon = document.getElementById('EyeIconRe');

        if(password.type === 'password'){
            password.type = 'text';
            eyeicon.src = "/assets/images/eye-open.png";
        }else{
            password.type = 'password';
            eyeicon.src = "/assets/images/eye-close.png";
        }
    }

  return (
        <aside id='RegisterForm'>
            <p>New Password</p>
            <p id='DetailRegister'>Silakan masukkan password baru Anda</p>
            <div id='InputGroup'>
                <label for="Password" id='InputLabel'>New Password</label>
                <div className='flex' id='InputPassword'>
                    <input type='password' id='InputForm' className='Password' name='Password' placeholder='•••••••••'></input>
                    <img src='/assets/images/eye-close.png' alt='' width='26px' id='EyeIcon' onClick={HandleEyeIcon}></img>
                </div>
            </div>
            <div id='InputGroup' className='mb-30'>
                <label for="Re-Password" id='InputLabel'>Confirm New Password</label>
                <div className='flex' id='InputPassword'>
                    <input type='password' id='InputForm' className='PasswordRe' name='Re-Password' placeholder='•••••••••'></input>
                    <img src='/assets/images/eye-close.png' alt='' width='26px' id='EyeIconRe' onClick={HandleEyeIconRe}></img>
                </div>
            </div>
            <button id='SignButton'>Reset Password</button>
            <div className='flex' id='qDSFE733Q'>
                <p>Sudah Punya Akun?</p>
                <p onClick={onLoginClick}>Sign In</p>
            </div>
        </aside>
  )
}

export default NewPasswordForm